# Multivendor Website Builder SaaS

## Core Features

Our platform is packed with powerful features to enhance your web development process:

- **Multivendor B2B2B SaaS**: Collaborate and transact within a multilayered business environment.
- **Agency and Sub Accounts**: Manage multiple projects or clients with dedicated accounts.
- **Unlimited Funnel Hosting**: Host as many marketing or sales funnels as you need.
- **Full Website & Funnel Builder**: Create stunning websites and funnels with our drag-and-drop builder.
- **Role-based Access**: Control access and permissions with customized roles for your team.
- **Stripe Subscription Plans**: Offer and manage subscription-based services with ease.
- **Stripe Add-on Products**: Expand your offerings with additional products and services.
- **Stripe Connect**: Seamlessly integrate Stripe accounts for all users, facilitating secure transactions.
- **Charge Application Fee Per Sale and Recurring Sales**: Monetize your platform by charging fees on transactions.
- **Custom Dashboards**: Gain insights with tailored dashboards for tracking performance and metrics.
- **Media Storage**: Store and manage digital assets with integrated media storage solutions.
- **Stripe Product Sync**: Keep your Stripe products and services synced with your platform for streamlined management.

## Getting Started

### Prerequisites

Before setting up the platform, ensure you have the following:

- Node.js (LTS version)
- MongoDB (For database storage)
- Stripe account (For payment processing)